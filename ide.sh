#!/bin/sh


# Launch a suitable IDE to edit the source code


src=rktsay
ide=drracket


if ! command -v ${ide} >/dev/null 2>&1
then
    echo "No suitable IDE found"
    echo "Please install ${ide}"
    exit 1
fi


${ide} ${src}/main.rkt >/dev/null 2>&1 &
